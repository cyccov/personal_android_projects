package com.mundodedafne.apps.miverduleriaapp.Activity;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.text.method.DigitsKeyListener;
import android.text.method.QwertyKeyListener;
import android.text.method.TextKeyListener;
import android.view.View;

import com.google.android.material.textfield.TextInputEditText;
import com.mundodedafne.apps.miverduleriaapp.R;
import com.mundodedafne.apps.miverduleriaapp.vo.NewProductVO;

public class NewProductActivity extends AppCompatActivity {

    TextInputEditText inputDescripcion = null;
    TextInputEditText inputPrecio = null;
    NewProductVO productVO = new NewProductVO();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_new_product);

        inputPrecio = (TextInputEditText) findViewById(R.id.textinput_et_precio_kilo);
        inputPrecio.setKeyListener(DigitsKeyListener.getInstance(false,true));
        productVO.setCostoProductoUnitario(Double.parseDouble(inputPrecio.getText().toString()));

        inputDescripcion = (TextInputEditText) findViewById(R.id.textinput_et_desc);
        inputDescripcion.setKeyListener(new QwertyKeyListener(TextKeyListener.Capitalize.SENTENCES, true));
    }
}