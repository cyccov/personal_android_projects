package com.mundodedafne.apps.miverduleriaapp.Activity;

import androidx.appcompat.app.AppCompatActivity;
import com.mundodedafne.apps.miverduleriaapp.R;
import android.os.Bundle;

public class AboutUsActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_about_us);
    }
}