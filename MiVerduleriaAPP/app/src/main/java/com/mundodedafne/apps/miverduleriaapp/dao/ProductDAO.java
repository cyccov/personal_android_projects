package com.mundodedafne.apps.miverduleriaapp.dao;

import com.mundodedafne.apps.miverduleriaapp.entity.Product;

import java.util.List;

public interface ProductDAO {
    public List<Product> findAllProducts();
}
