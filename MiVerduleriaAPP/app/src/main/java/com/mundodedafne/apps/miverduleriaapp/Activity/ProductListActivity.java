package com.mundodedafne.apps.miverduleriaapp.Activity;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;

import com.mundodedafne.apps.miverduleriaapp.R;

public class ProductListActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_product_list);
    }
}