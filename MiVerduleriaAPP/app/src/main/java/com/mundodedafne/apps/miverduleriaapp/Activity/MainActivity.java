package com.mundodedafne.apps.miverduleriaapp.Activity;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

import com.mundodedafne.apps.miverduleriaapp.R;

public class MainActivity extends AppCompatActivity {

    //APP para las verdulerias
    //Permite agregar productos a temporales, así como registrar las ventas del día
    //a día de las verdulerías.

    private Button buttonNewProduct, buttonSeasons;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        buttonNewProduct = (Button) findViewById(R.id.button_new_product);
        buttonSeasons = (Button) findViewById(R.id.button_seasons);

        buttonSeasons.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                MainActivity.this.startActivity(new Intent(MainActivity.this, SeasonsActivity.class));
            }
        });

        buttonNewProduct.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                MainActivity.this.startActivity(new Intent(MainActivity.this, NewProductActivity.class));
            }
        });
    }
}