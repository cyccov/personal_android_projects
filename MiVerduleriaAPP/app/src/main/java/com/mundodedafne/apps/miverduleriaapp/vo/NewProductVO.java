package com.mundodedafne.apps.miverduleriaapp.vo;

public class NewProductVO {
    private String descripcionProducto;
    private double costoProductoUnitario;
    private double costoTotal;

    public String getDescripcionProducto() {
        return descripcionProducto;
    }

    public void setDescripcionProducto(String descripcionProducto) {
        this.descripcionProducto = descripcionProducto;
    }

    public double getCostoProductoUnitario() {
        return costoProductoUnitario;
    }

    public void setCostoProductoUnitario(double costoProductoUnitario) {
        this.costoProductoUnitario = costoProductoUnitario;
    }

    public double getCostoTotal() {
        return costoTotal;
    }

    public void setCostoTotal(double costoTotal) {
        this.costoTotal = costoTotal;
    }
}
